PROTWIS
=======

Welcome to the Protwis project, the technology behind [GPCRdb](http://gpcrdb.org) and related systems.

For more information, please refer to our [documentation](http://docs.gpcrdb.org).

INSTALLATION
------------

The recommended method of installation is using our Vagrant configuration.

Please follow the [instructions in the documentation](http://docs.gpcrdb.org/local_installation.html#for-development).
